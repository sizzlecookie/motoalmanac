# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151112171445) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "general_specs", force: :cascade do |t|
    t.integer  "vehicle_id"
    t.string   "engine_type"
    t.string   "engine_displacement"
    t.string   "bore_stroke"
    t.string   "cooling"
    t.string   "compression_ratio"
    t.string   "fuel_system"
    t.string   "ignition"
    t.string   "starting_system"
    t.string   "transmission"
    t.string   "final_drive"
    t.string   "rake_trail"
    t.string   "seat_height"
    t.string   "wheelbase"
    t.string   "front_suspension"
    t.string   "rear_suspension"
    t.string   "front_brake"
    t.string   "rear_brake"
    t.string   "front_tire"
    t.string   "rear_tire"
    t.string   "fuel_capacity"
    t.string   "dry_weight"
    t.integer  "msrp"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "general_specs", ["vehicle_id"], name: "index_general_specs_on_vehicle_id", using: :btree

  create_table "manufacturers", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quick_specs", force: :cascade do |t|
    t.string   "oil_change_capacity"
    t.string   "oil_with_filter_capacity"
    t.string   "oil_overhaul_capacity"
    t.string   "oil_weight"
    t.string   "oil_recommended"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "vehicle_id"
    t.string   "stock_suspension_forks"
    t.string   "stock_suspension_shock"
    t.string   "valve_clearance"
  end

  add_index "quick_specs", ["vehicle_id"], name: "index_quick_specs_on_vehicle_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.integer  "position"
    t.integer  "manufacturer_id"
    t.string   "year"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "category_id"
  end

  add_index "vehicles", ["category_id"], name: "index_vehicles_on_category_id", using: :btree
  add_index "vehicles", ["manufacturer_id"], name: "index_vehicles_on_manufacturer_id", using: :btree

end
