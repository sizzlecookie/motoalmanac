class CreateManufacturers < ActiveRecord::Migration
  def change
    create_table :manufacturers do |t|
      t.string :name
      t.boolean :active
      t.integer :position

      t.timestamps null: false
    end
  end
end
