class CleanUpTables < ActiveRecord::Migration
  def change
    remove_column("general_specs", "manufacturer_id")
    remove_column("general_specs", "category_id")
    remove_column("quick_specs", "general_spec_id")
    add_column("vehicles", "category_id", :integer)
    add_column("quick_specs", "vehicle_id", :integer)
    add_index("vehicles", "manufacturer_id")
    add_index("vehicles", "category_id")
    add_index("quick_specs", "vehicle_id")
    add_index("general_specs", "vehicle_id")
    add_column("quick_specs", "stock_suspension_forks", :string)
    add_column("quick_specs", "stock_suspension_shock", :string)
    add_column("quick_specs", "valve_clearance", :string)


  end
end
