class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :name
      t.boolean :active
      t.integer :position
      t.integer :manufacturer_id
      t.string :year

      t.timestamps null: false
    end
  end
end
