class CreateGeneralSpecs < ActiveRecord::Migration
  def change
    create_table :general_specs do |t|
      t.integer :manufacturer_id
      t.integer :vehicle_id
      t.integer :category_id
      t.string :engine_type
      t.string :engine_displacement
      t.string :bore_stroke
      t.string :cooling
      t.string :compression_ratio
      t.string :fuel_system
      t.string :ignition
      t.string :starting_system
      t.string :transmission
      t.string :final_drive
      t.string :rake_trail
      t.string :seat_height
      t.string :wheelbase
      t.string :front_suspension
      t.string :rear_suspension
      t.string :front_brake
      t.string :rear_brake
      t.string :front_tire
      t.string :rear_tire
      t.string :fuel_capacity
      t.string :dry_weight
      t.integer :msrp

      t.timestamps null: false
    end
  end
end
