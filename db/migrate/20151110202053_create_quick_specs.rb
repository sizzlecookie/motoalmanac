class CreateQuickSpecs < ActiveRecord::Migration
  def change
    create_table :quick_specs do |t|
      t.integer :general_spec_id
      t.string :oil_change_capacity
      t.string :oil_with_filter_capacity
      t.string :oil_overhaul_capacity
      t.string :oil_weight
      t.string :oil_recommended

      t.timestamps null: false
    end
  end
end
