require 'test_helper'

class QuickSpecsControllerTest < ActionController::TestCase
  setup do
    @quick_spec = quick_specs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quick_specs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quick_spec" do
    assert_difference('QuickSpec.count') do
      post :create, quick_spec: { general_spec_id: @quick_spec.general_spec_id, oil_change_capacity: @quick_spec.oil_change_capacity, oil_overhaul_capacity: @quick_spec.oil_overhaul_capacity, oil_recommended: @quick_spec.oil_recommended, oil_weight: @quick_spec.oil_weight, oil_with_filter_capacity: @quick_spec.oil_with_filter_capacity }
    end

    assert_redirected_to quick_spec_path(assigns(:quick_spec))
  end

  test "should show quick_spec" do
    get :show, id: @quick_spec
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quick_spec
    assert_response :success
  end

  test "should update quick_spec" do
    patch :update, id: @quick_spec, quick_spec: { general_spec_id: @quick_spec.general_spec_id, oil_change_capacity: @quick_spec.oil_change_capacity, oil_overhaul_capacity: @quick_spec.oil_overhaul_capacity, oil_recommended: @quick_spec.oil_recommended, oil_weight: @quick_spec.oil_weight, oil_with_filter_capacity: @quick_spec.oil_with_filter_capacity }
    assert_redirected_to quick_spec_path(assigns(:quick_spec))
  end

  test "should destroy quick_spec" do
    assert_difference('QuickSpec.count', -1) do
      delete :destroy, id: @quick_spec
    end

    assert_redirected_to quick_specs_path
  end
end
