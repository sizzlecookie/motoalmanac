require 'test_helper'

class GeneralSpecsControllerTest < ActionController::TestCase
  setup do
    @general_spec = general_specs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:general_specs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create general_spec" do
    assert_difference('GeneralSpec.count') do
      post :create, general_spec: { bore_stroke: @general_spec.bore_stroke, category_id: @general_spec.category_id, compression_ratio: @general_spec.compression_ratio, cooling: @general_spec.cooling, dry_weight: @general_spec.dry_weight, engine_displacement: @general_spec.engine_displacement, engine_type: @general_spec.engine_type, final_drive: @general_spec.final_drive, front_brake: @general_spec.front_brake, front_suspension: @general_spec.front_suspension, front_tire: @general_spec.front_tire, fuel_capacity: @general_spec.fuel_capacity, fuel_system: @general_spec.fuel_system, ignition: @general_spec.ignition, manufacturer_id: @general_spec.manufacturer_id, msrp: @general_spec.msrp, rake_trail: @general_spec.rake_trail, rear_brake: @general_spec.rear_brake, rear_suspension: @general_spec.rear_suspension, rear_tire: @general_spec.rear_tire, seat_height: @general_spec.seat_height, starting_system: @general_spec.starting_system, transmission: @general_spec.transmission, vehicle_id: @general_spec.vehicle_id, wheelbase: @general_spec.wheelbase }
    end

    assert_redirected_to general_spec_path(assigns(:general_spec))
  end

  test "should show general_spec" do
    get :show, id: @general_spec
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @general_spec
    assert_response :success
  end

  test "should update general_spec" do
    patch :update, id: @general_spec, general_spec: { bore_stroke: @general_spec.bore_stroke, category_id: @general_spec.category_id, compression_ratio: @general_spec.compression_ratio, cooling: @general_spec.cooling, dry_weight: @general_spec.dry_weight, engine_displacement: @general_spec.engine_displacement, engine_type: @general_spec.engine_type, final_drive: @general_spec.final_drive, front_brake: @general_spec.front_brake, front_suspension: @general_spec.front_suspension, front_tire: @general_spec.front_tire, fuel_capacity: @general_spec.fuel_capacity, fuel_system: @general_spec.fuel_system, ignition: @general_spec.ignition, manufacturer_id: @general_spec.manufacturer_id, msrp: @general_spec.msrp, rake_trail: @general_spec.rake_trail, rear_brake: @general_spec.rear_brake, rear_suspension: @general_spec.rear_suspension, rear_tire: @general_spec.rear_tire, seat_height: @general_spec.seat_height, starting_system: @general_spec.starting_system, transmission: @general_spec.transmission, vehicle_id: @general_spec.vehicle_id, wheelbase: @general_spec.wheelbase }
    assert_redirected_to general_spec_path(assigns(:general_spec))
  end

  test "should destroy general_spec" do
    assert_difference('GeneralSpec.count', -1) do
      delete :destroy, id: @general_spec
    end

    assert_redirected_to general_specs_path
  end
end
