class Manufacturer < ActiveRecord::Base
  has_many :vehicles, inverse_of: :manufacturer
  has_many :categories, through: :vehicles

  validates_presence_of :name
  validates_length_of :name, :maximum => 255
end
