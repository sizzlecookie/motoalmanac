class Vehicle < ActiveRecord::Base
  has_one :general_spec
  has_one :quick_spec
  belongs_to :manufacturer, inverse_of: :vehicles
  belongs_to :category, inverse_of: :vehicles

  validates_presence_of :name
  validates_length_of :name, :maximum => 255
  # after_destroy :delete_related_quick_spec
  # after_destroy :delete_related_general_spec
  #
  # def delete_related_quick_spec
  #   self.quick_specs.each do |quick_spec|
  #     # Or perhaps instead of destroy, you would
  #     # move them to another page.
  #     # sections destroy
  #   end
  # end
  #
  # def delete_related_general_spec
  #   self.general_specs.each do |general_spec|
  #     # Or perhaps instead of destroy, you would
  #     # move them to another page.
  #     # sections destroy
  #   end
  # end
  #TODO: Delete Orphans

end
