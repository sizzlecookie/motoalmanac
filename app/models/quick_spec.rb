class QuickSpec < ActiveRecord::Base
  belongs_to :vehicle

  # acts_as_list :scope => :vehicle
  #TODO: add acts_as_list gem
  #TODO: add scopes (once we get data and shit gets confusing)

  after_save :touch_vehicle

  private

  def touch_vehicle
    # touch is similar to:
    # subject.update_attribute(:updated_at, Time.now)
    vehicle.touch
  end
end
