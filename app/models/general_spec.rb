class GeneralSpec < ActiveRecord::Base
  belongs_to :vehicle

  # acts_as_list :scope => :vehicle
  #TODO: add acts_as_list gem

  after_save :touch_vehicle

  private

  def touch_vehicle
    # touch is similar to:
    # subject.update_attribute(:updated_at, Time.now)
    vehicle.touch
  end
end
