class Category < ActiveRecord::Base
  has_many :vehicles, inverse_of: :category
  has_many :manufacturers, through: :vehicles

  validates_presence_of :name
  validates_length_of :name, :maximum => 255

end
