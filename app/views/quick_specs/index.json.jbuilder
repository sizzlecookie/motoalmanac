json.array!(@quick_specs) do |quick_spec|
  json.extract! quick_spec, :id, :general_spec_id, :oil_change_capacity, :oil_with_filter_capacity, :oil_overhaul_capacity, :oil_weight, :oil_recommended
  json.url quick_spec_url(quick_spec, format: :json)
end
