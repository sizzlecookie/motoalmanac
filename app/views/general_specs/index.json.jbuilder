json.array!(@general_specs) do |general_spec|
  json.extract! general_spec, :id, :manufacturer_id, :vehicle_id, :category_id, :engine_type, :engine_displacement, :bore_stroke, :cooling, :compression_ratio, :fuel_system, :ignition, :starting_system, :transmission, :final_drive, :rake_trail, :seat_height, :wheelbase, :front_suspension, :rear_suspension, :front_brake, :rear_brake, :front_tire, :rear_tire, :fuel_capacity, :dry_weight, :msrp
  json.url general_spec_url(general_spec, format: :json)
end
