json.array!(@categories) do |category|
  json.extract! category, :id, :name, :active, :position
  json.url category_url(category, format: :json)
end
