json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :name, :active, :position, :manufacturer_id, :year
  json.url vehicle_url(vehicle, format: :json)
end
