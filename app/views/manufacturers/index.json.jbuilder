json.array!(@manufacturers) do |manufacturer|
  json.extract! manufacturer, :id, :name, :active, :position
  json.url manufacturer_url(manufacturer, format: :json)
end
