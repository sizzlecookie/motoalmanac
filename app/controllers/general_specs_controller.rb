class GeneralSpecsController < ApplicationController

  layout 'admin'

  before_action :confirm_logged_in
  before_action :set_general_spec, only: [:show, :edit, :update, :destroy]

  # GET /general_specs
  # GET /general_specs.json
  def index
    @general_specs = GeneralSpec.all
  end

  # GET /general_specs/1
  # GET /general_specs/1.json
  def show
  end

  # GET /general_specs/new
  def new
    @general_spec = GeneralSpec.new
  end

  # GET /general_specs/1/edit
  def edit
  end

  # POST /general_specs
  # POST /general_specs.json
  def create
    @general_spec = GeneralSpec.new(general_spec_params)

    respond_to do |format|
      if @general_spec.save
        format.html { redirect_to @general_spec, notice: 'General spec was successfully created.' }
        format.json { render :show, status: :created, location: @general_spec }
      else
        format.html { render :new }
        format.json { render json: @general_spec.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /general_specs/1
  # PATCH/PUT /general_specs/1.json
  def update
    respond_to do |format|
      if @general_spec.update(general_spec_params)
        format.html { redirect_to @general_spec, notice: 'General spec was successfully updated.' }
        format.json { render :show, status: :ok, location: @general_spec }
      else
        format.html { render :edit }
        format.json { render json: @general_spec.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /general_specs/1
  # DELETE /general_specs/1.json
  def destroy
    @general_spec.destroy
    respond_to do |format|
      format.html { redirect_to general_specs_url, notice: 'General spec was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general_spec
      @general_spec = GeneralSpec.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_spec_params
      params.require(:general_spec).permit(:manufacturer_id, :vehicle_id, :category_id, :engine_type, :engine_displacement, :bore_stroke, :cooling, :compression_ratio, :fuel_system, :ignition, :starting_system, :transmission, :final_drive, :rake_trail, :seat_height, :wheelbase, :front_suspension, :rear_suspension, :front_brake, :rear_brake, :front_tire, :rear_tire, :fuel_capacity, :dry_weight, :msrp)
    end
end
