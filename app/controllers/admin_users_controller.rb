class AdminUsersController < ApplicationController

  layout 'admin'
  before_action :confirm_logged_in

  def index
    @admin_users = AdminUser.sorted
  end

  def new
    @admin_user = AdminUser.new
  end

  def create
    @admin_user = AdminUser.new(admin_user_params)
    if @admin_user.save
      flash[:notice] = 'New Admin User Created Successfully!'
      redirect_to(action: 'index')
    else
      render('new')
    end
  end

  def edit
    @admin_user = AdminUser.find(params[:id])
  end

  def update
    @admin_user = AdminUser.find(params[:id])

    if @admin_user.update_attributes(admin_user_params)
      flash[:notice] = "Admin User Updated Successfully!"
      redirect_to(action: 'index')
    else
      render('edit')
    end
  end

  def delete
    @admin_user = AdminUser.find(params[:id])
  end

  def destroy
    admin_user = AdminUser.find(params[:id]).destroy
    flash[:notice] = "'#{admin_user.email}' Deleted"
    redirect_to(action: 'index')
  end

  private

  def admin_user_params
    params.require(:admin_user).permit(:email, :encrypted_password, :reset_password_token, :sign_in_count, :current_sign_in_ip, :last_sign_in_ip, :updated_at)
  end



end
