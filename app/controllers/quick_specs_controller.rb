class QuickSpecsController < ApplicationController

  layout 'admin'

  before_action :confirm_logged_in
  before_action :set_quick_spec, only: [:show, :edit, :update, :destroy]

  # GET /quick_specs
  # GET /quick_specs.json
  def index
    @quick_specs = QuickSpec.all
  end

  # GET /quick_specs/1
  # GET /quick_specs/1.json
  def show
  end

  # GET /quick_specs/new
  def new
    @quick_spec = QuickSpec.new
  end

  # GET /quick_specs/1/edit
  def edit
  end

  # POST /quick_specs
  # POST /quick_specs.json
  def create
    @quick_spec = QuickSpec.new(quick_spec_params)

    respond_to do |format|
      if @quick_spec.save
        format.html { redirect_to @quick_spec, notice: 'Quick spec was successfully created.' }
        format.json { render :show, status: :created, location: @quick_spec }
      else
        format.html { render :new }
        format.json { render json: @quick_spec.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quick_specs/1
  # PATCH/PUT /quick_specs/1.json
  def update
    respond_to do |format|
      if @quick_spec.update(quick_spec_params)
        format.html { redirect_to @quick_spec, notice: 'Quick spec was successfully updated.' }
        format.json { render :show, status: :ok, location: @quick_spec }
      else
        format.html { render :edit }
        format.json { render json: @quick_spec.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quick_specs/1
  # DELETE /quick_specs/1.json
  def destroy
    @quick_spec.destroy
    respond_to do |format|
      format.html { redirect_to quick_specs_url, notice: 'Quick spec was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quick_spec
      @quick_spec = QuickSpec.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quick_spec_params
      params.require(:quick_spec).permit(:general_spec_id, :oil_change_capacity, :oil_with_filter_capacity, :oil_overhaul_capacity, :oil_weight, :oil_recommended)
    end
end
